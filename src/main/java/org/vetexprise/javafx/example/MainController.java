package org.vetexprise.javafx.example;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 *
 * @author developer
 */
public class MainController {

    @FXML
    private Label status;

    @FXML
    private void add() throws IOException {
        status.setText("Сохранение ...");
    }

     @FXML
    private void delete() throws IOException {
        status.setText("Удаление ...");
    }
    
}
